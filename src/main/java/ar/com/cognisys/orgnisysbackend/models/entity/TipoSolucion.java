package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "tipo_solucion")
public class TipoSolucion {
    @Id
    @Column(name = "id_tipo_solucion", nullable = false)
    private Long idTipoSolucion;

    @Column(name= "desc_solucion")
    private String solucion;


    public Long getIdTipoSolucion() {
        return idTipoSolucion;
    }

    public void setIdTipoSolucion(Long idTipoSolucion) {
        this.idTipoSolucion = idTipoSolucion;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }
}
