package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "tipo_tarjeta")
public class TipoTarjeta {
    @Id
    @Column(name = "id_tipo", nullable = false)
    private Long id;

    @Column(name = "tipo", nullable = false)
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
