package ar.com.cognisys.orgnisysbackend.models.response;

import ar.com.cognisys.orgnisysbackend.models.entity.Incidente;
import ar.com.cognisys.orgnisysbackend.models.entity.Tarjeta;

import java.util.List;

public class GetIncidentesResponse {

    private List<Incidente> incidentes;

    public GetIncidentesResponse(List<Incidente> incidentes) {
        this.incidentes = incidentes;
    }

    public List<Incidente> getIncidentes() {
        return incidentes;
    }

    public void setIncidentes(List<Incidente> incidentes) {
        this.incidentes = incidentes;
    }
}
