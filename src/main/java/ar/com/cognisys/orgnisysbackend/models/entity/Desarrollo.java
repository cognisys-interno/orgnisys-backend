package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "tarjeta_desarrollo")
public class Desarrollo {
    @Id
    @Column(name = "id_desarrollo", nullable = false)
    private Long id_desarrollo;

    @OneToMany
    @JoinColumn(name = "id_tarjeta")
    private List<Tarjeta> tarjeta;

    @Column(name= "pruebas_estimadas")
    private Integer pruebasEstimadas;

    @Column(name= "pruebas_realizadas")
    private Integer pruebasRealizadas;

    @Column(name= "pruebas_reportadas")
    private Integer pruebasReportadas;

    @Column(name= "horas_estimadas")
    private double horasEstimadas;

    @Column(name= "horas_reales")
    private double horasReales;

    public Long getId_desarrollo() {
        return id_desarrollo;
    }

    public void setId_desarrollo(Long id_desarrollo) {
        this.id_desarrollo = id_desarrollo;
    }

    public List<Tarjeta> getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(List<Tarjeta> tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Integer getPruebasEstimadas() {
        return pruebasEstimadas;
    }

    public void setPruebasEstimadas(Integer pruebasEstimadas) {
        this.pruebasEstimadas = pruebasEstimadas;
    }

    public Integer getPruebasRealizadas() {
        return pruebasRealizadas;
    }

    public void setPruebasRealizadas(Integer pruebasRealizadas) {
        this.pruebasRealizadas = pruebasRealizadas;
    }

    public Integer getPruebasReportadas() {
        return pruebasReportadas;
    }

    public void setPruebasReportadas(Integer pruebasReportadas) {
        this.pruebasReportadas = pruebasReportadas;
    }

    public double getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(double horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public double getHorasReales() {
        return horasReales;
    }

    public void setHorasReales(double horasReales) {
        this.horasReales = horasReales;
    }
}
