package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="tarjeta")
public class Tarjeta {
    @Id
    @Column(name = "ID_TARJETA", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TITULO")
    private String titulo;

    @ManyToOne
    @JoinColumn(name = "id_tipo")
    private TipoTarjeta tipoTarjeta;

    @ManyToOne
    @JoinColumn(name = "id_estado")
    private Estado estado;

    @Column(name = "USUARIO_ASIGNADO")
    private String usuarioAsignado;

    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Column(name = "link_externo")
    private String link;

    @Column(name = "data_source")
    private boolean dataSource;

    @Column(name = "data_base")
    private boolean dataBase;

    @Column(name = "logger")
    private boolean logger;

    @Column(name = "eficacia")
    private boolean eficacia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public TipoTarjeta getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getUsuarioAsignado() {
        return usuarioAsignado;
    }

    public void setUsuarioAsignado(String usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isDataSource() {
        return dataSource;
    }

    public void setDataSource(boolean dataSource) {
        this.dataSource = dataSource;
    }

    public boolean isDataBase() {
        return dataBase;
    }

    public void setDataBase(boolean dataBase) {
        this.dataBase = dataBase;
    }

    public boolean isLogger() {
        return logger;
    }

    public void setLogger(boolean logger) {
        this.logger = logger;
    }

    public boolean isEficacia() {
        return eficacia;
    }

    public void setEficacia(boolean eficacia) {
        this.eficacia = eficacia;
    }
}
