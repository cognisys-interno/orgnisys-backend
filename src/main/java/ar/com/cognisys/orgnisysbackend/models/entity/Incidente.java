package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name = "tarjeta_incidente")
public class Incidente {
    @Id
    @Column(name = "id_incidente", nullable = false)
    private Long idIncidente;

    @OneToMany
    @JoinColumn(name = "id_tarjeta")
    private List<Tarjeta> tarjeta;

    @ManyToOne
    @JoinColumn(name = "id_tipo_incidencia")
    private TipoIncidencia tipoIncidencia;

    @ManyToOne
    @JoinColumn(name = "id_tipo_solucion")
    private TipoSolucion tipoSolucion;

    @Column(name = "solucion")
    private String solucion;

    public Long getIdIncidente() {
        return idIncidente;
    }

    public void setIdIncidente(Long idIncidente) {
        this.idIncidente = idIncidente;
    }

    public List<Tarjeta> getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(List<Tarjeta> tarjeta) {
        this.tarjeta = tarjeta;
    }

    public TipoIncidencia getTipoIncidencia() {
        return tipoIncidencia;
    }

    public void setTipoIncidencia(TipoIncidencia tipoIncidencia) {
        this.tipoIncidencia = tipoIncidencia;
    }

    public TipoSolucion getTipoSolucion() {
        return tipoSolucion;
    }

    public void setTipoSolucion(TipoSolucion tipoSolucion) {
        this.tipoSolucion = tipoSolucion;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }
}
