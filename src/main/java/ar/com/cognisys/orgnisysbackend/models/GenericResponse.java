package ar.com.cognisys.orgnisysbackend.models;

import org.springframework.http.HttpStatus;

public class GenericResponse {

    private HttpStatus responseCode;
    private String responseMessage;

    public GenericResponse(HttpStatus responseCode, String responseMessage) {
        super();
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public HttpStatus getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(HttpStatus responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
