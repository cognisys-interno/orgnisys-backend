package ar.com.cognisys.orgnisysbackend.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "tipo_incidencia")
public class TipoIncidencia {
    @Id
    @Column(name = "id_tipo_incidencia", nullable = false)
    private Long idTipoIncidencia;

    @Column(name= "desc_incidencia")
    private String descripcion;


    public Long getIdTipoIncidencia() {
        return idTipoIncidencia;
    }

    public void setIdTipoIncidencia(Long id_tipo_incidencia) {
        this.idTipoIncidencia = id_tipo_incidencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
