package ar.com.cognisys.orgnisysbackend.controller;

import ar.com.cognisys.orgnisysbackend.models.entity.Incidente;
import ar.com.cognisys.orgnisysbackend.models.response.GetIncidentesResponse;
import ar.com.cognisys.orgnisysbackend.service.IncidenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/incidentes")
@CrossOrigin
public class IncidenteController {

    @Autowired
    private final IncidenteService incidenteService;

    public IncidenteController(IncidenteService incidenteService) {
        this.incidenteService = incidenteService;
    }


    @GetMapping(path = "/getIncidentes", produces = "application/json")
    public ResponseEntity<GetIncidentesResponse> getTarjetas(){
        GetIncidentesResponse response = new GetIncidentesResponse(incidenteService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}