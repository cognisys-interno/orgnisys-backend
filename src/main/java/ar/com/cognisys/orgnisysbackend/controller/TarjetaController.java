package ar.com.cognisys.orgnisysbackend.controller;

import ar.com.cognisys.orgnisysbackend.models.GenericResponse;
import ar.com.cognisys.orgnisysbackend.models.entity.Tarjeta;
import ar.com.cognisys.orgnisysbackend.service.TarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TarjetaController {

    @Autowired
    private final TarjetaService tarjetaService;

    public TarjetaController(TarjetaService tarjetaService) {
        this.tarjetaService = tarjetaService;
    }

    @GetMapping(path = "/getTarjetas", produces = "application/json")
    public ResponseEntity<List<Tarjeta>> getTarjetas(){
        return new ResponseEntity<>(tarjetaService.findAll(),HttpStatus.OK);
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Tarjeta crear(@RequestBody Tarjeta producto){
        return tarjetaService.save(producto);
    }

}
