package ar.com.cognisys.orgnisysbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrgnisysBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrgnisysBackendApplication.class, args);
	}

}
