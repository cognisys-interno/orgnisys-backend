package ar.com.cognisys.orgnisysbackend.repository;

import ar.com.cognisys.orgnisysbackend.models.entity.Tarjeta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITarjetaRespository extends JpaRepository<Tarjeta, Long> {



}
