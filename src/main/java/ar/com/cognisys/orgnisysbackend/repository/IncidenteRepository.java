package ar.com.cognisys.orgnisysbackend.repository;

import ar.com.cognisys.orgnisysbackend.models.entity.Incidente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IncidenteRepository extends JpaRepository<Incidente,Long> {

    public List<Incidente> findByTipoIncidenciaIsNot(String titulo);


}
