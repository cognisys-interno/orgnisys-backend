package ar.com.cognisys.orgnisysbackend.service;

import ar.com.cognisys.orgnisysbackend.models.entity.Incidente;
import ar.com.cognisys.orgnisysbackend.repository.IncidenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidenteService  {

    @Autowired
    private final IncidenteRepository IncidenteRepository;

    public IncidenteService(ar.com.cognisys.orgnisysbackend.repository.IncidenteRepository incidenteRepository) {
        IncidenteRepository = incidenteRepository;
    }


    public List<Incidente> findAll() {
        return IncidenteRepository.findAll();
    }

    public List<Incidente> findByTitulo(String titulo) {
        return null;
    }


}
