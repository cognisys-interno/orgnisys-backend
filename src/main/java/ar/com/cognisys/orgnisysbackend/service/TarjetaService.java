package ar.com.cognisys.orgnisysbackend.service;

import ar.com.cognisys.orgnisysbackend.repository.ITarjetaRespository;
import ar.com.cognisys.orgnisysbackend.models.entity.Desarrollo;
import ar.com.cognisys.orgnisysbackend.models.entity.Incidente;
import ar.com.cognisys.orgnisysbackend.models.entity.Tarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarjetaService{

    @Autowired
    private final ITarjetaRespository tarjetaDao;

    public TarjetaService(ITarjetaRespository tarjetaDao) {
        this.tarjetaDao = tarjetaDao;
    }

    public List<Tarjeta> findAll() {
        return (List<Tarjeta>)  tarjetaDao.findAll();
    }

    public Tarjeta save(Tarjeta tarjeta) {
        return null;
    }

    public List<Desarrollo> findAllDesarrollos() {
        return null;
    }


}
